/*
 *	Scope - A simple and minimal metasearch engine
 *	Copyright (C) 2021 Arsen Musayelyan
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package web

import (
	"net/http"
	"strconv"

	"github.com/PuerkitoBio/goquery"
)

var bingURL = urlMustParse("https://www.bing.com/search?count=10")

// Bing represents the Bing search engine
type Bing struct {
	keyword   string
	userAgent string
	first     int
	doc       *goquery.Document
	initDone  bool
	baseSel   *goquery.Selection
}

// SetKeyword sets the keyword for searching
func (b *Bing) SetKeyword(keyword string) {
	b.keyword = keyword
}

// SetPage sets the page number for searching
func (b *Bing) SetPage(page int) {
	b.first = page * 10
}

// SetUserAgent sets the user agent to use for the request
func (b *Bing) SetUserAgent(ua string) {
	b.userAgent = ua
}

// Init runs requests for Bing search engine
func (b *Bing) Init() error {
	// Copy URL so it can be changed
	initURL := copyURL(bingURL)
	query := initURL.Query()
	// Set query
	query.Set("q", b.keyword)
	if b.first > 0 {
		query.Set("first", strconv.Itoa(b.first))
	} else {
		query.Set("first", "1")
	}
	// Update URL query parameters
	initURL.RawQuery = query.Encode()

	// Create new request for modified URL
	req, err := http.NewRequest(
		http.MethodGet,
		initURL.String(),
		nil,
	)
	if err != nil {
		return err
	}
	// If no user agent, use default
	if b.userAgent == "" {
		b.userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
	}
	// Set request user agent
	req.Header.Set("User-Agent", b.userAgent)

	// Perform request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// Create new goquery document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return err
	}
	b.doc = doc
	b.baseSel = doc.Find(`#b_results > li`)
	b.initDone = true
	return nil
}

// Each runs eachCb with the index of each search result
func (b *Bing) Each(eachCb func(int) error) error {
	for i := 0; i < b.baseSel.Length(); i++ {
		err := eachCb(i)
		if err != nil {
			return err
		}
	}
	return nil
}

// Title returns the title of the search result corresponding to i
func (b *Bing) Title(i int) (string, error) {
	return get(b.baseSel, i).ChildrenFiltered("h2").Children().First().Text(), nil
}

// Link returns the link to the search result corresponding to i
func (b *Bing) Link(i int) (string, error) {
	return get(b.baseSel, i).ChildrenFiltered("h2").Children().First().AttrOr("href", ""), nil
}

// Desc returns the description of the search result corresponding to i
func (b *Bing) Desc(i int) (string, error) {
	return get(b.baseSel, i).ChildrenFiltered(".b_caption").Children().Last().Text(), nil
}

// Name returns "bing"
func (b *Bing) Name() string {
	return "bing"
}
