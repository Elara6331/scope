package web_test

import (
	"errors"
	"testing"

	"go.arsenm.dev/scope/search/web"
)

const (
	ErrInNone = iota
	ErrInInit
	ErrInLink
	ErrInTitle
	ErrInDesc
)

var (
	ErrInit  = errors.New("error in init")
	ErrLink  = errors.New("error in link")
	ErrTitle = errors.New("error in title")
	ErrDesc  = errors.New("error in description")
)

type TestEngine struct {
	errIn     int
	name      string
	query     string
	userAgent string
	page      int
	results   []TestResult
}

type TestResult struct {
	title string
	link  string
	desc  string
}

func (te *TestEngine) SetKeyword(query string) {
	te.query = query
}

func (te *TestEngine) SetUserAgent(ua string) {
	te.userAgent = ua
}

func (te *TestEngine) SetPage(page int) {
	te.page = page
}

func (te *TestEngine) Init() error {
	if te.errIn == ErrInInit {
		return ErrInit
	}
	te.results = append(te.results,
		TestResult{
			"Google",
			"https://www.google.com",
			"Google search engine",
		},
		TestResult{
			"Wikipedia",
			"https://wikipedia.org",
			"Online wiki encyclopedia",
		},
		TestResult{
			"Reddit",
			"https://reddit.com",
			"600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars 600 chars ",
		},
		TestResult{
			"Example",
			"https://example.com",
			"",
		},
	)
	return nil
}

func (te *TestEngine) Each(eachCb func(int) error) error {
	for index := range te.results {
		if err := eachCb(index); err != nil {
			return err
		}
	}
	return nil
}

func (te *TestEngine) Title(i int) (string, error) {
	if te.errIn == ErrInTitle {
		return "", ErrTitle
	}
	return te.results[i].title, nil
}

func (te *TestEngine) Link(i int) (string, error) {
	if te.errIn == ErrInLink {
		return "", ErrLink
	}
	return te.results[i].link, nil
}

func (te *TestEngine) Desc(i int) (string, error) {
	if te.errIn == ErrInDesc {
		return "", ErrDesc
	}
	return te.results[i].desc, nil
}

func (te *TestEngine) Name() string {
	return te.name
}

func TestSearch(t *testing.T) {
	engine := &TestEngine{name: "one"}

	results, err := web.Search(
		web.Options{
			Keyword:   "test keyword",
			UserAgent: "TestEngine/0.0.0",
			Page:      0,
		},
		engine,
		&TestEngine{name: "two"},
	)
	if err != nil {
		t.Fatalf("Error in Search(): %s", err)
	}

	if len(results) < len(engine.results)-1 {
		t.Fatalf(
			"Expected %d results, got %d",
			len(engine.results),
			len(results),
		)
	}

	for index, result := range results {
		if engine.results[index].desc == "" {
			continue
		}
		if result.Title != engine.results[index].title {
			t.Fatalf(
				"Result %d: expected title %s, got %s",
				index,
				engine.results[index].title,
				result.Title,
			)
		} else if result.Link != engine.results[index].link {
			t.Fatalf(
				"Result %d: expected link %s, got %s",
				index,
				engine.results[index].link,
				result.Link,
			)
		} else if result.Desc != engine.results[index].desc &&
			len(engine.results[index].desc) <= 500 {
			t.Fatalf(
				"Result %d: expected description %s, got %s",
				index,
				engine.results[index].desc,
				result.Desc,
			)
		}
	}
}

func TestSearchError(t *testing.T) {
	engines := []*TestEngine{
		{errIn: ErrInInit},
		{errIn: ErrInTitle},
		{errIn: ErrInLink},
		{errIn: ErrInDesc},
	}

	for index, engine := range engines {
		_, err := web.Search(
			web.Options{
				Keyword:   "test keyword",
				UserAgent: "TestEngine/0.0.0",
				Page:      0,
			},
			engine,
		)
		if err == nil {
			t.Fatalf("Expected error in engine %d, received nil", index)
		} else if err != ErrTitle && engines[index].errIn == ErrInTitle {
			t.Fatalf("Expected error in title (index %d), received %s", index, err)
		} else if err != ErrLink && engines[index].errIn == ErrInLink {
			t.Fatalf("Expected error in link (index %d), received %s", index, err)
		} else if err != ErrDesc && engines[index].errIn == ErrInDesc {
			t.Fatalf("Expected error in description (index %d), received %s", index, err)
		} else if err != ErrInit && engines[index].errIn == ErrInInit {
			t.Fatalf("Expected error in init (index %d), received %s", index, err)
		}
	}
}
