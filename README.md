# Scope
A simple and minimal metasearch engine

[![Go Report Card](https://goreportcard.com/badge/go.arsenm.dev/scope)](https://goreportcard.com/report/go.arsenm.dev/scope)

---

## Why?

I created Scope because I felt an easy, simple metasearch engine did not exist. I was using searx, but it requires many external programs and resources, and it can often be very complex to set up, especially if not used on x86_64.

---

## Features

Scope has the following features:

- Per-request user agent randomization
- Result cards
    - DuckDuckGo Instant Answers
    - Equation solver
    - Equation graphing
    - Location weather
- Global config

---

## Result Cards

When searching for certain queries, a card can appear in the results. This card will contain the answer to the query. These cards work by detecting key words in the query.

### Calculation

The calculation card is a frontend card that solves a given equation or expression. The following queries can be used for calculations:

- Expressions
    - `calculate <expression>`
- Equations
    - `solve <equation>`
    - `solve <equation> for <variable>`
- Integrals
    - `integrate <expression>`
    - `integral <expression>`
    - `integral of <expression>`
- Derivatives
    - `diff <expression>`
    - `derive <expression>`
    - `differentiate <expression>`
    - `derivative <expression>`
    - `derivative of <expression>`

### Plot

The plot card is a frontend card that graphs a given function. The following keywords can be used for this:

- `plot <equation>`
- `graph <equation>`
- `draw <equation> `

### Weather

The weather card is a backend card that gets results from metaweather. The following keywords can be used:

- `weather <location>`
- `weather in <location>`
- `<location> weather`

### DuckDuckGo

This card is a backend card that gets results from the DuckDuckGo Instant Answer API. It gets tried if no other card matches.

---

## Config

The config for Scope applies to all users of the instance. There is no settings UI, as that would massively increase complexity, and the point of Scope is to be simple and minimal.

The config allows you to configure a lot most of Scope.

---

## Static Resources

Scope's static resources are embedded in the binary. Therefore, all that's needed to run scope is the binary and the config. This does not use Cgo, so cross compiling is as easy as setting the `GOARCH` and `GOOS` libraries. While only tested on Linux, it should work on other OSes as well.

---

## Mentions

This project uses many other projects. Those projects and the reasons for using them will be listed here:

- [Bulma](https://bulma.io): CSS framework
- [Nerdamer](https://nerdamer.com/): Equation solver
- [Function Plot](https://mauriciopoppe.github.io/function-plot/): Equation grapher
- [Metaweather](https://www.metaweather.com/): Weather API
- [DuckDuckGo](https://duckduckgo.com/): Provides instant answers
- [Dark Reader](https://darkreader.org/): Dark mode CSS