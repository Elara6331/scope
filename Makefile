all: build static
.PHONY: all

build:
	go build
.PHONY: build

static:
	mkdir -p static/ext

	wget https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css -O static/ext/bulma.min.css

	wget https://code.iconify.design/2/2.1.0/iconify.min.js -O static/ext/iconify.min.js
	
	wget https://cdn.jsdelivr.net/npm/katex@0.15.1/dist/katex.min.css -O static/ext/katex.min.css
	wget https://cdn.jsdelivr.net/npm/katex@0.15.1/dist/katex.min.js -O static/ext/katex.min.js

	wget https://cdn.jsdelivr.net/npm/nerdamer@latest/nerdamer.core.js -O static/ext/nerdamer.core.js
	wget https://cdn.jsdelivr.net/npm/nerdamer@latest/Algebra.js -O static/ext/Algebra.js
	wget https://cdn.jsdelivr.net/npm/nerdamer@latest/Calculus.js -O static/ext/Calculus.js
	wget https://cdn.jsdelivr.net/npm/nerdamer@latest/Solve.js -O static/ext/Solve.js

	wget https://unpkg.com/function-plot/dist/function-plot.js -O static/ext/function-plot.js