/*
 *	Scope - A simple and minimal metasearch engine
 *	Copyright (C) 2021 Arsen Musayelyan
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cards

import (
	"fmt"
	"html/template"
	"strings"
)

const plotExtraHead = `
<script src="/static/ext/function-plot.js"></script>
<style>
.top-right-legend {
	display: none;
}
</style>`

const plotScript = `
<div id="plot-content" class="container"></div>
<script>
// Create function to draw plot in #plot-content
plotFn = () => functionPlot({
	target: '#plot-content',
	grid: true,
	width: document.getElementById('plot-content').offsetWidth,
	data: [{
		fn: '%s'
	}]
})
// Create resize observer that runs plot function
new ResizeObserver(plotFn).observe(document.getElementById('plot-content'))
</script>`

func init() {
	// Register plot card
	Register("plot", 2, NewPlotCard)
}

// PlotCard represents a card with an equation plot
type PlotCard struct {
	query string
}

// NewPlotCard is a NewCardFunc that creates a new PlotCard
func NewPlotCard(query, _ string) Card {
	return &PlotCard{query: query}
}

// Matches checks if the query matches the rules for PlotCard
func (pc *PlotCard) Matches() bool {
	return strings.HasPrefix(pc.query, "plot") ||
		strings.HasPrefix(pc.query, "graph") ||
		strings.HasPrefix(pc.query, "draw")
}

// StripKey removes all keys related to PlotCard
func (pc *PlotCard) StripKey() string {
	query := strings.TrimPrefix(pc.query, "plot")
	query = strings.TrimPrefix(query, "graph")
	query = strings.TrimPrefix(query, "draw")
	return strings.TrimSpace(query)
}

// Content returns plot script with given input
func (pc *PlotCard) Content() template.HTML {
	return template.HTML(fmt.Sprintf(
		plotScript,
		pc.StripKey(),
	))
}

// Returned will alwats return true because
// this card is frontend, and this cannot be checked.
func (pc *PlotCard) Returned() bool {
	return true
}

// Title generates a title formatted as "Plot (<eqation>)"
func (pc *PlotCard) Title() string {
	return "Plot (" + pc.StripKey() + ")"
}

// Head returns extra head tags for PlotCard
func (pc *PlotCard) Head() template.HTML {
	return plotExtraHead
}

// Footer returns an empty string as PlotCard has no footer
func (pc *PlotCard) Footer() template.HTML {
	return ""
}

// RunQuery returns nil as PlotCard is a frontend card
func (pc *PlotCard) RunQuery() error {
	return nil
}
